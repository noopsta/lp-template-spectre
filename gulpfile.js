var gulp = require('gulp');
var sass = require('gulp-sass');
var php = require('gulp-connect-php');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

// Basic Gulp task syntax
gulp.task('hello', function() {
  console.log('Hello Zell!');
})


gulp.task('php', function() {
    php.server({ base: 'app', port: 8010, keepalive: true});
});

// Development Tasks 
// -----------------

// Start browserSync server
gulp.task('browserSync',['php'], function() {
   browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });
});

gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass().on('error', sass.logError)) // Passes it through a gulp-sass, log errors to console
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(gulp.dest('app/css')) // Outputs it in the css folder
    .pipe(browserSync.reload({ // Reloading with Browser Sync
      stream: true
    }));
})

// Watchers
gulp.task('watch', function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.php', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
})

// Optimization Tasks 
// ------------------




// Optimizing CSS and JavaScript 
gulp.task('useref', function() {

  return gulp.src('app/*.php')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'));
});

// Task to minify HTML
gulp.task('htmlmin', function() {
  return gulp.src('dist/*.php')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('min'));
});


// Optimizing Images 
gulp.task('images', function() {
  return gulp.src('app/img/**/*.+(png|jpg|jpeg|gif|svg)')
    // Caching images that ran through imagemin
    .pipe((imagemin({ optimizationLevel: 9, interlaced: true, progressive: true})))
    .pipe(gulp.dest('dist/img'))
});


// Copying awesome 
gulp.task('awesome', function() {
  return gulp.src('app/font-awesome/**/*')
    .pipe(gulp.dest('dist/font-awesome'))
})


// Copying fonts 
gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
})


// Copying awesome 
gulp.task('awesome', function() {
  return gulp.src('app/css/**/*')
    .pipe(gulp.dest('dist/css'))
})

// Copying includes 
gulp.task('includes', function() {
  return gulp.src('app/js/**/*')
    .pipe(gulp.dest('dist/js'))
})

// Cleaning 
gulp.task('clean', function() {
  return del.sync('dist').then(function(cb) {
    return cache.clearAll(cb);
  });
})

gulp.task('clean:dist', function() {
  return del.sync(['dist/**/*', '!dist/img', '!dist/img/**/*']);
});

// Build Sequences
// ---------------

gulp.task('default', function(callback) {
  runSequence(['sass', 'browserSync'], 'watch',
    callback
  )
})

gulp.task('build', function(callback) {
  runSequence(
    'clean:dist',
    'sass',
    ['useref','images', 'fonts', 'awesome', 'includes'],
    callback
  )
})

