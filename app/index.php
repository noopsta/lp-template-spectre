<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Trauringe Mainz Altstadt  - Trauringe Mainz</title>
	<!--build:css css/style.min.css-->
	<link rel="stylesheet" href="css/app.css">
	<!--endbuild-->

</head>



<body>



				<div class="off-canvas">
				  <!-- off-screen toggle button -->
				  <a class="off-canvas-toggle btn btn-primary btn-action" href="#sidebar-id">
				    <i class="icon icon-menu"></i>
				  </a>

				  <div id="sidebar-id" class="off-canvas-sidebar">

				  	 <a href="#close" class="float-right px-2 py-2"> <i class="icon icon-cross"></i></a>


				    <!-- off-screen sidebar -->
				    	<ul class="nav">
						  <li class="nav-item">
						    <a href="#">Elements</a>
						  </li>
						  <li class="nav-item active">
						    <a href="#">Layout</a>
						    <ul class="nav">
						      <li class="nav-item">
						        <a href="#">Flexbox grid</a>
						      </li>
						      <li class="nav-item">
						        <a href="#">Responsive</a>
						      </li>
						      <li class="nav-item">
						        <a href="#">Navbar</a>
						      </li>
						      <li class="nav-item">
						        <a href="#">Empty states</a>
						      </li>
						    </ul>
						  </li>
						  <li class="nav-item">
						    <a href="#">Components</a>
						  </li>
						  <li class="nav-item">
						    <a href="#">Utilities</a>
						  </li>
						</ul>

				  </div>

				  <a class="off-canvas-overlay" href="#close"></a>

				  <div class="off-canvas-content">
				    <!-- off-screen content -->
				  </div>
				</div>


<div class="container grid-xl">
  	<div class="columns">
  	   <div class="column col-12">
			<h1>Trauringe Mainz</h1>
			<h2>Individuelle Trauringschmieden aus Mainz</h2>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


			<p><img src="img/trauringe-aus-mainz.jpg" class="img-responsive" alt="Trauringe Mainz">

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

			<h3>Handgemachte Trauringe</h3>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 	 </div>
  </div>
</div>

</body>
</html>